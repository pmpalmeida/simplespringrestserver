# Simple Spring REST Server

A simple RESTful service to test the Angular/React application.

## Getting Started

Simply open the project in your IDE, update maven dependencies and run.