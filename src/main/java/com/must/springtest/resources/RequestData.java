package com.must.springtest.resources;

public class RequestData {

    private Number number1, number2;

    public void setNumber1(Number number1) {
        this.number1 = number1;
    }

    public void setNumber2(Number number2) {
        this.number2 = number2;
    }

    public Number getN1() {
        return number1;
    }

    public Number getN2() {
        return number2;
    }
}
