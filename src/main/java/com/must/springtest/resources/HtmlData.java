package com.must.springtest.resources;

public class HtmlData {
    private String html;

    public void setHtml(String html) {
        this.html = html;
    }

    public String getHtml() {
        return html;
    }
}
