package com.must.springtest.resources;

public class ResultData {

    private double result;

    public ResultData(double result) {
        this.result = result;
    }

    public double getResult() {
        return result;
    }
}
