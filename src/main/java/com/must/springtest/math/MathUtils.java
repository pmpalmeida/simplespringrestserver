package com.must.springtest.math;

import com.must.springtest.resources.RequestData;

public class MathUtils {

    public static double add(RequestData data) throws NullPointerException {
        return data.getN1().doubleValue() + data.getN2().doubleValue();
    }

    public static double subtract(RequestData data) throws NullPointerException {
        return data.getN1().doubleValue() - data.getN2().doubleValue();
    }

    public static double multiply(RequestData data) throws NullPointerException {
        return data.getN1().doubleValue() * data.getN2().doubleValue();
    }

    public static double divide(RequestData data) throws NullPointerException {
        return data.getN1().doubleValue() / data.getN2().doubleValue();
    }
}
