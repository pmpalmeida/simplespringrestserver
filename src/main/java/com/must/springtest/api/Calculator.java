package com.must.springtest.api;

import com.must.springtest.resources.RequestData;
import com.must.springtest.resources.ResultData;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = "*")
@RequestMapping("/calc")
public interface Calculator {

    //TEST METHOD - Testing the injection of code directly into the application
    @RequestMapping(value = "/html", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
    String html();

    @RequestMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<ResultData> add(@RequestBody RequestData data);

    @RequestMapping(value = "/minus", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<ResultData> subtract(@RequestBody RequestData data);

    @RequestMapping(value = "/mult", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<ResultData> multiply(@RequestBody RequestData data);

    @RequestMapping(value = "/div", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ResponseEntity<ResultData> divide(@RequestBody RequestData data);
}
