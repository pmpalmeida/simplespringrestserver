package com.must.springtest.controllers;

import com.must.springtest.api.Pokemon;
import com.must.springtest.resources.PokemonData;
import com.must.springtest.resources.ResultData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class PokemonController implements Pokemon {

    private static final Logger LOG = Logger.getLogger(PokemonController.class.getName());

    public ResponseEntity<String> getAll() {

        try {
            JSONArray pokemonArray = getAllPokemon(false);
            return new ResponseEntity<>(pokemonArray.toJSONString(), HttpStatus.OK);

        } catch(Exception e) {
            LOG.log(Level.WARNING, e.getMessage());
            return defaultExceptionResponse();
        }
    }

    public ResponseEntity<String> getAllSorted() {

        try {
            JSONArray pokemonArray = getAllPokemon(true);
            return new ResponseEntity<>(pokemonArray.toJSONString(), HttpStatus.OK);

        } catch(Exception e) {
            LOG.log(Level.WARNING, e.getMessage());
            return defaultExceptionResponse();
        }
    }

    public boolean addNewPokemon(PokemonData data) {
        JSONObject pokemonDetails = new JSONObject();
        pokemonDetails.put("name", data.getName());
        pokemonDetails.put("type", data.getType());
        pokemonDetails.put("src", data.getSrc());
        pokemonDetails.put("description", data.getDescription());
        return false;
    }

    private JSONArray getAllPokemon(boolean sort) throws Exception {
        JSONParser jsonParser = new JSONParser();

        FileReader reader = new FileReader("C:/Users/palmeida/Documents/Repositories/simplespringrestserver/src/assets/data/pokemon.json");
        Object obj = jsonParser.parse(reader);

        JSONArray pokemons = (JSONArray) obj;
        if(sort) {
            pokemons = sort(pokemons);
        }

        return pokemons;
    }

    private JSONArray sort(JSONArray toSort) {
        JSONArray sortedPokemons = new JSONArray();

        List<JSONObject> jsonValues = new ArrayList<>();
        for (int i = 0; i < toSort.size(); i++) {
            jsonValues.add((JSONObject) toSort.get(i));
        }
        jsonValues.sort(new Comparator<>() {
            private static final String KEY_NAME = "name";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = (String) a.get(KEY_NAME);
                String valB = (String) b.get(KEY_NAME);

                return valA.compareTo(valB);
            }
        });

        for(int i = 0; i < toSort.size(); i++) {
            sortedPokemons.add(jsonValues.get(i));
        }

        return sortedPokemons;
    }

    private ResponseEntity<String> defaultExceptionResponse() {
        return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
    }
}
