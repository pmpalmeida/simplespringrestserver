package com.must.springtest.controllers;

import com.must.springtest.api.Calculator;
import com.must.springtest.math.MathUtils;
import com.must.springtest.resources.RequestData;
import com.must.springtest.resources.ResultData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class CalculatorController implements Calculator {

    private static final Logger LOG = Logger.getLogger(Calculator.class.getName());

    public String html() {
        return("<p style=\"color: \"white\";\">" +
                "Html from Server" +
                "</p>");
    }

    public ResponseEntity<ResultData> add(@RequestBody RequestData data) {
        LOG.log(Level.INFO, "Attempting adding "+ data.getN1() +" with " + data.getN2());
        try {
            return new ResponseEntity<>(new ResultData(MathUtils.add(data)), HttpStatus.OK);
        } catch (NullPointerException e) {
            LOG.log(Level.WARNING, "Missing Arguments...");
            return defaultExceptionResponse();
        }
    }

    public ResponseEntity<ResultData> subtract(@RequestBody RequestData data) {
        LOG.log(Level.INFO, "Attempting subtracting "+ data.getN2() +" from " + data.getN1());
        try {
            return new ResponseEntity<>(new ResultData(MathUtils.subtract(data)), HttpStatus.OK);
        } catch(NullPointerException e) {
            LOG.log(Level.WARNING, "Missing Arguments...");
            return defaultExceptionResponse();
        }
    }

    public ResponseEntity<ResultData> multiply(@RequestBody RequestData data) {
        LOG.log(Level.INFO, "Attempting multiplying "+ data.getN1() +" by " + data.getN2());
        try {
            return new ResponseEntity<>(new ResultData(MathUtils.multiply(data)), HttpStatus.OK);
        } catch(NullPointerException e) {
            LOG.log(Level.WARNING, "Missing Arguments...");
            return defaultExceptionResponse();
        }
    }

    public ResponseEntity<ResultData> divide(@RequestBody RequestData data) {
        LOG.log(Level.INFO, "Attempting dividing "+ data.getN1() +" by " + data.getN2());
        try {
            return new ResponseEntity<>(new ResultData(MathUtils.divide(data)), HttpStatus.OK);
        } catch(NullPointerException e) {
            LOG.log(Level.WARNING, "Missing Arguments...");
            return defaultExceptionResponse();
        }
    }

    private ResponseEntity<ResultData> defaultExceptionResponse() {
        return new ResponseEntity<>(new ResultData(0), HttpStatus.BAD_REQUEST);
    }
}
